import {
  ChakraProvider
} from "@chakra-ui/react"

import {
  Provider
} from "react-redux"

import {
  store,
  persistedStore
} from "./store/store"

import { PersistGate } from "redux-persist/integration/react"

import ShopListView from "./components/ShopListView/ShopListView.component"

function App() {
  return (
    <Provider store={store}>
    <PersistGate loading={null} persistor={persistedStore}>
    <ChakraProvider>
      <ShopListView/>
    </ChakraProvider>
    </PersistGate>
    </Provider>
  );
}

export default App;
