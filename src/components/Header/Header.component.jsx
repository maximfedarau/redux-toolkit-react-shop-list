import React from "react"

import {
    Center,
    Text
} from "@chakra-ui/react"

export default function Header() {
    return (
        <Center w="100%" bg="black" color="white" height="60px">
            <Text fontSize="25px">Shop List</Text>
        </Center>
    )
}