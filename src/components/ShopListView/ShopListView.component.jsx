import React from 'react'

import {
    Center,
    Box,
    Grid
} from "@chakra-ui/react"

import ShopOptions from "../ShopOptions/ShopOptions.component"
import ShopCart from '../ShopCart/ShopCart.component'
import Header from '../Header/Header.component'

export default function ShopListView() {

    return (
        <Box>
            <Header/>
            <Center>
            <Box>
                <Grid gridAutoFlow="column" gap={10} marginTop="10px">
                <ShopCart/>
                <ShopOptions/>
                </Grid>
            </Box>
            </Center>
        </Box>
    )
}