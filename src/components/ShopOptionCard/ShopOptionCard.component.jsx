import React from 'react'

import {
    Button,
    Text,
    Grid,
} from "@chakra-ui/react"

import {
    useDispatch
} from "react-redux"

import {
    shopListActions
} from "../../store/shopList/shopList.slice"

import {
    SHOP_LIST_REDUCER_TYPES
} from "../../store/shopList/shopList.config"

export default function ShopOptionCard({option}) {

    const dispatch = useDispatch()
    const addItem = shopListActions[SHOP_LIST_REDUCER_TYPES.addItem]

    const [addItemButtonVisibility,setAddItemButonVisibility] = React.useState(false)

    function onMouseOverCardHandler() {
        setAddItemButonVisibility(true)
    }

    function onMouseLeaveCardHandler() {
        setAddItemButonVisibility(false)
    }

    function onClickAddItemHandler() {
        dispatch(addItem(option))
    }

    return (
        <Grid
        onMouseOver={onMouseOverCardHandler}
        onMouseLeave={onMouseLeaveCardHandler} 
        border="1px solid black" 
        borderRadius="1rem"
        padding="10px"
        width="200px"
        textAlign="center"
        gridAutoFlow="column">
            {(addItemButtonVisibility) && <Button
            height="25px"
            onClick={onClickAddItemHandler}>Add</Button>}
            <Text>{option}</Text>
        </Grid>
    )
}