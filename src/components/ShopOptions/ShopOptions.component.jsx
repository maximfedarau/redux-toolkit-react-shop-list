import React from "react"

import shopOptionsListJSON from "../../shopOptionsList.json"

import ShopOptionCard from "../ShopOptionCard/ShopOptionCard.component"

import {
    Box
} from "@chakra-ui/react"

export default function ShopOptions() {

    const shopOptionsList = shopOptionsListJSON.shopOptionsList

    return (
        <Box>
           {shopOptionsList.map((value) => {
               return <ShopOptionCard key={value} option={value}/>
           })}
        </Box>
    )
}