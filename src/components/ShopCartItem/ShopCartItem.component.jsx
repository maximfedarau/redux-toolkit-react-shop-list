import React from "react"

import {
    Text, 
    Box,
    Button,
    Grid
} from "@chakra-ui/react"

import {
    useDispatch
} from "react-redux"

import {
    shopListActions
} from "../../store/shopList/shopList.slice"

import {
    SHOP_LIST_REDUCER_TYPES
} from "../../store/shopList/shopList.config"

export default function ShopCartItem({itemName,itemCount}) {

    const dispatch  = useDispatch()

    const incrementCounter = shopListActions[SHOP_LIST_REDUCER_TYPES.addItem]
    const decrementCounter = shopListActions[SHOP_LIST_REDUCER_TYPES.decrementCounter]
    const removeItem = shopListActions[SHOP_LIST_REDUCER_TYPES.removeItem]

    function onClickIncrementHandler() {
        dispatch(incrementCounter(itemName))
    }

    function onClickDecrementHandler() {
        dispatch(decrementCounter(itemName))
    }

    function onClickRemoveHandler() {
        dispatch(removeItem(itemName))
    }

    return (
        <Grid border="1px solid black" gridAutoFlow="column" padding="10px" borderRadius="1rem">
             <Text>{itemName}</Text>&nbsp;
             <Button height="25px" onClick={onClickDecrementHandler}>{"<"}</Button>&nbsp;
             <Text>{itemCount}</Text>&nbsp;
             <Button height="25px" onClick={onClickIncrementHandler}>{">"}</Button>&nbsp;
             <Button height="25px" onClick={onClickRemoveHandler}>x</Button>
        </Grid>
    )
}