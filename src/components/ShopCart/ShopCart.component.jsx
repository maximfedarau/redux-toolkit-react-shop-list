import React from "react"

import {
    useSelector
} from "react-redux"

import {
    Box,
    Text
} from "@chakra-ui/react"

import {
    shopListSelector
} from "../../store/shopList/shopList.selector"

import ShopCartItem from "../ShopCartItem/ShopCartItem.component"

export default function ShopCart() {

    const shopList = useSelector(shopListSelector)

    return (
        <Box>
            {shopList.map((item) => <ShopCartItem key={item.name} itemName={item.name} itemCount={item.count}/>)}
        </Box>
    )
}