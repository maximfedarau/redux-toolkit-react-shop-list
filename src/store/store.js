import {
    configureStore,
    combineReducers
} from "@reduxjs/toolkit"

import {
    shopListReducer
} from "./shopList/shopList.slice"

import {
    logger
} from "redux-logger"

import storage from "redux-persist/lib/storage"
import {
    persistReducer, persistStore
} from "redux-persist"

const middleware = [process.env.NODE_ENV !== "production" && logger].filter(Boolean)
const enhacers = [...middleware]

const rootReducer = combineReducers({shopList: shopListReducer})

const persistConfig = {
    key: 'root',
    storage
}

const persistedReducer = persistReducer(persistConfig,rootReducer)

export const store = configureStore({
    reducer: persistedReducer,
    middleware: enhacers,
    devTools: false
})

export const persistedStore = persistStore(store)