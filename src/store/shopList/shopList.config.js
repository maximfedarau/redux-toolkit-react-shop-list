export const initialState = {
    shopList: []
}

export const SHOP_LIST_SLICE_NAME = "shopList"

export const SHOP_LIST_REDUCER_TYPES = {
    addItem: "addItem",
    decrementCounter: "decrementCounter",
    removeItem: "removeItem"
}