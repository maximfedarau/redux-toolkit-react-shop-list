import {
    createSlice
} from '@reduxjs/toolkit'

import {
    initialState,
    SHOP_LIST_SLICE_NAME,
    SHOP_LIST_REDUCER_TYPES,
} from "./shopList.config"

function removeItemHelper(shopList,itemName) {
    return shopList.filter((item) => {
        return item.name !== itemName
    })
}

const shopListSlice = createSlice({
    name: SHOP_LIST_SLICE_NAME,
    initialState,
    reducers: {
        [SHOP_LIST_REDUCER_TYPES.addItem]: (state,action) => {
            let isExists = false
            state.shopList.map((item) => {
                if (item.name === action.payload) {
                    item.count+=1
                    isExists = true
                }
            })
            if (!isExists) {
                state.shopList = state.shopList.concat([{name: action.payload, count:1}])

            }
        },
        [SHOP_LIST_REDUCER_TYPES.decrementCounter]: (state,action) => {
            state.shopList.map((item) => {
                if (item.name === action.payload) {
                    if (item.count === 1) {
                        state.shopList = removeItemHelper(state.shopList,action.payload)
                        return 
                    }
                    item.count-=1
                    return 
                } 
            })
        },
        [SHOP_LIST_REDUCER_TYPES.removeItem]: (state,action) => {
            state.shopList = removeItemHelper(state.shopList,action.payload)
        }
    }
})

export const shopListReducer = shopListSlice.reducer

export const shopListActions = shopListSlice.actions

