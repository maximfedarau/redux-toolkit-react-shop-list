import {
    createSelector
} from "@reduxjs/toolkit"

const selectorFromShopListReducer = (state) => state.shopList

export const shopListSelector = createSelector(
    [selectorFromShopListReducer],
    (info) => info.shopList
)